//
//  NetworkingManager.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

enum NetworkingManagerResponseError: String {
    
    case errorParse = "Unable to parse data"
    case errorNoData = "No data found"
    
}

class NetworkingManager {

    static var shared = NetworkingManager()

    func downloadItems(callback: @escaping ([ItemModel]?, NetworkingManagerResponseError?) -> Void ) {
      
        request(filename: "Items") { data in

            guard let data = data else { callback(nil, .errorParse); return }
            
            do {
                
                guard let jsonObjectSerialized = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any],
                    let data = jsonObjectSerialized["data"]
                else { callback(nil, .errorParse); return }

                let objectArray = try JSONSerialization.data(withJSONObject: data, options: [])
                let result = try JSONDecoder().decode([ItemModel].self, from: objectArray)
                
                callback(result, nil)
                
            } catch let error {
                print(error)
                callback(nil, .errorParse)
            }
         
        }
        
    }
    
    func downloadItem(withId id: String, callback: @escaping (ItemDetailsModel?, NetworkingManagerResponseError?) -> Void) {
        
        request(filename: "Item" + id) { data in
            
            guard let data = data else { callback(nil, .errorParse); return }
            
            do {
                
                guard let jsonObjectSerialized = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any],
                    let data = jsonObjectSerialized["data"]
                else { callback(nil, .errorParse); return }
                
                let objectArray = try JSONSerialization.data(withJSONObject: data, options: [])
                let result = try JSONDecoder().decode(ItemDetailsModel.self, from: objectArray)
                
                callback(result, nil)
                
            } catch let error {
                print(error)
                callback(nil, .errorParse)
            }
            
        }
        
    }
    
    private func request(filename:String, completionBlock: @escaping (Data?) -> Void) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            if let data = JSONParser.json(from: filename) {
                completionBlock(data)
            } else {
                completionBlock(nil)
            }
        }
        
    }
    
}
