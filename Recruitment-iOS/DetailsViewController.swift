//
//  DetailsViewController.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController, DetailsViewModelDelegate {
    
    @IBOutlet weak var textView: UITextView!
    
    let viewModel = DetailsViewViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = viewModel.selectedColor
        title = viewModel.formattedTitle
        
        viewModel.delegate = self
        viewModel.getData()
        
    }
    
    // MARK: - DetailsViewModelDelegate functions
    
    func dataLoad() {
        textView.text = viewModel.dataItem?.desc
    }
    
    func dataLoadError() {
        textView.text = "Unable to load data"
    }

}
