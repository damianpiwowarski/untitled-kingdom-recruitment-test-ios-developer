//
//  ColorViewController.swift
//  Recruitment-iOS
//
//  Created by Damian Piwowarski on 17/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import Foundation
import UIKit

class ColorViewController: UICollectionViewController, TableViewModelDelegate, UICollectionViewDelegateFlowLayout {

    let viewModel = ColorViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.register(UINib(nibName: ColorCollectionViewCell.stringName, bundle: nil), forCellWithReuseIdentifier: ColorCollectionViewCell.stringName)
        
        if let flowLayout = collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.minimumLineSpacing = 0
            flowLayout.minimumInteritemSpacing = 0
            flowLayout.sectionInsetReference = .fromSafeArea
        }
        
        viewModel.delegate = self
        viewModel.getData()
        
    }
    
    // MARK: - TableViewModelDelegate functions
    
    func dataLoad() {
        
        DispatchQueue.main.async { [weak self] in
            self?.collectionView.reloadData()
        }
        
    }
    
    func dataLoadError() {}
    
    // MARK: UICollectionViewDelegateFlowLayout functions
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        guard let _ = collectionViewLayout as? UICollectionViewFlowLayout else {
            let cellSize = collectionView.safeAreaLayoutGuide.layoutFrame.width
            return CGSize(width: cellSize, height: cellSize)
        }

        let columns: CGFloat = 2
        let cellSize = collectionView.safeAreaLayoutGuide.layoutFrame.width / columns
        
        return CGSize(width: cellSize, height: cellSize)
        
    }
    
    // MARK: - CollectionView functions
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        guard let data = viewModel.data else { return 0 }
        return data.count
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ColorCollectionViewCell.stringName, for: indexPath) as? ColorCollectionViewCell else { return UICollectionViewCell() }
        guard let dataItem = viewModel.data?.get(indexPath.row) else { return UICollectionViewCell() }
        
        cell.configure(with: dataItem)
        
        return cell
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "ShowDetailsSegue", sender: indexPath.row)
    }
    
    // MARK: - ColorViewController functions
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: { [weak self]  transition in
            
            if let flowLayout = self?.collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                flowLayout.invalidateLayout()
            }
            
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "ShowDetailsSegue", let destinationViewController = segue.destination as? DetailsViewController {
            
            guard let selectedIndexPath = collectionView.indexPathsForSelectedItems?.get(0) else { return }
            guard let dataItem = viewModel.data?.get(selectedIndexPath.row) else { return }
            
            destinationViewController.viewModel.id = dataItem.id
            destinationViewController.viewModel.selectedColor = dataItem.attributes.color.uicolor
            destinationViewController.viewModel.selectedItemName = dataItem.attributes.name
            
        }
        
    }
    
}
