//
//  Extensions.swift
//  Recruitment-iOS
//
//  Created by Damian Piwowarski on 17/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import Foundation

extension NSObject {
    
    class var stringName: String {
        return String(describing: self)
    }
    
}

extension Array {
    
    func get(_ index: Int) -> Element? {
        if index >= 0 && index < count {
            return self[index]
        } else {
            return nil
        }
    }
    
}
