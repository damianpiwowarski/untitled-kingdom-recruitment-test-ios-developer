//
//  DetailsCell.swift
//  Recruitment-iOS
//
//  Created by Damian Piwowarski on 17/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import Foundation
import UIKit

class DetailsCell: UITableViewCell {
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    
    func configure(with dataItem: ItemModel) {
        
        backgroundColor = dataItem.color
        labelTitle.text = dataItem.name
        
        if let preview = dataItem.preview {
            labelDescription.text = preview
            labelDescription.isHidden = false
        } else {
            labelDescription.isHidden = true
        }
        
    }

}
