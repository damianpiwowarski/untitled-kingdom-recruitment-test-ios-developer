//
//  JSONParser.swift
//  Route1
//
//  Created by Paweł Sporysz on 11.12.2015.
//  Copyright © 2015 Untitled Kingdom. All rights reserved.
//

import Foundation

class JSONParser {
    
    static func json(from filename:String) -> Data? {
        guard let filepath = Bundle.main.path(forResource: filename, ofType: "json") else { return nil }
        do {
            let stringContent = try String(contentsOfFile: filepath, encoding: String.Encoding.utf8)
            return stringContent.data(using: String.Encoding.utf8)
        } catch _ {}
        return nil
    }
    
}
