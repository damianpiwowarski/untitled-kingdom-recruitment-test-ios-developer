//
//  DetailsViewViewModel.swift
//  Recruitment-iOS
//
//  Created by Damian Piwowarski on 17/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import Foundation
import UIKit

protocol DetailsViewModelDelegate: AnyObject {
    func dataLoad()
    func dataLoadError()
}

class DetailsViewViewModel {

    var selectedColor: UIColor?
    var selectedItemName: String?
    
    var formattedTitle: String? {

        return selectedItemName?.enumerated().reduce("") { word, item in
            return word + ( item.offset % 2 != 0 ? item.element.lowercased() : item.element.uppercased() )
        }
        
    }
    
    var id: String?
    var dataItem: ItemDetailsModel?
    
    weak var delegate: DetailsViewModelDelegate?
    
    func getData() {
        
        guard let id = self.id else { delegate?.dataLoadError(); return }
        
        NetworkingManager.shared.downloadItem(withId: id) { [weak self] data, error in
            
            guard let strongSelf = self else { return }
            guard let data = data else { strongSelf.delegate?.dataLoadError(); return }
            
            strongSelf.dataItem = data
            strongSelf.delegate?.dataLoad()
            
        }
        
    }

}
