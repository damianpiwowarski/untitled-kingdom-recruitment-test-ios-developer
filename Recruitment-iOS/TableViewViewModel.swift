//
//  TableViewViewModel.swift
//  Recruitment-iOS
//
//  Created by Damian Piwowarski on 17/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import Foundation

protocol TableViewModelDelegate: AnyObject {
    func dataLoad()
    func dataLoadError()
}

class TableViewViewModel {

    var data: [ItemModel]?
    weak var delegate: TableViewModelDelegate?
    
    func getData() {
       
        NetworkingManager.shared.downloadItems() { [weak self] data, error in
            
            guard let strongSelf = self else { return }
            guard let data = data else { strongSelf.delegate?.dataLoadError(); return }
            
            strongSelf.data = data
            strongSelf.delegate?.dataLoad()
            
        }
        
    }

}
