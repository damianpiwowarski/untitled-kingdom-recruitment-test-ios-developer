//
//  ColorCollectionViewCell.swift
//  Recruitment-iOS
//
//  Created by Damian Piwowarski on 17/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import Foundation
import UIKit

class ColorCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var labelTitle: UILabel!
    
    func configure(with dataItem: ItemModel) {
        
        backgroundColor = dataItem.color
        labelTitle.text = dataItem.name
        
    }
    
}
