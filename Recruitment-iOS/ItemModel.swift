//
//  ItemModel.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

struct Color: Codable {
    
    var name: String
    var uicolor : UIColor
    
    init(name: String, uiColor: UIColor) {
        self.name = name
        self.uicolor = uiColor
    }
    
    init(from decoder: Decoder) throws {
        
        let mappings = [ "Red" : UIColor.red, "Green" : UIColor.green, "Blue": UIColor.blue, "Yellow": UIColor.yellow, "Purple": UIColor.purple ]
        
        let container = try decoder.singleValueContainer()
        name = try container.decode(String.self)

        uicolor = mappings[name] ?? UIColor.black
        
    }
    
    func encode(to encoder: Encoder) throws {
        
        var container = encoder.singleValueContainer()
        try container.encode(name)
        
    }
    
}

class ItemModelAttributes: Codable {
    
    let name: String
    let color: Color
    let preview: String?
    let desc: String?
    
    init(name: String, color: Color, preview: String?, desc: String?) {
        self.name = name
        self.color = color
        self.preview = preview
        self.desc = desc
    }
    
}

class ItemModel: Codable {
    
    var id: String
    var type: String
    var attributes: ItemModelAttributes
    
    var color: UIColor {
        return attributes.color.uicolor
    }
    
    var preview: String? {
        return attributes.preview
    }
    
    var name: String {
        return attributes.name
    }
    
    init(id: String, type: String, attributes: ItemModelAttributes) {
        self.id = id
        self.type = type
        self.attributes = attributes
    }
    
}
