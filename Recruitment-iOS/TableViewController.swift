//
//  TableViewController.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController, TableViewModelDelegate {
    
    let viewModel = TableViewViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: DetailsCell.stringName, bundle: nil), forCellReuseIdentifier: DetailsCell.stringName)
        
        viewModel.delegate = self
        viewModel.getData()
        
    }

    // MARK: - TableViewModelDelegate functions
    
    func dataLoad() {
        
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.tableView.reloadData()
        }
        
    }
    
    func dataLoadError() {}
    
    // MARK: - TableView functions
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let data = viewModel.data else { return 0 }
        return data.count
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: DetailsCell.stringName, for: indexPath) as? DetailsCell else { return UITableViewCell() }
        guard let dataItem = viewModel.data?.get(indexPath.row) else { return UITableViewCell() }
        
        cell.configure(with: dataItem)

        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let _ = viewModel.data?.get(indexPath.row) else { return }
        performSegue(withIdentifier: "ShowDetailsSegue", sender: indexPath.row)
    }
    
    // MARK: - TableViewController functions
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "ShowDetailsSegue", let destinationViewController = segue.destination as? DetailsViewController {
            
            guard let selectedIndexPath = tableView.indexPathForSelectedRow else { return }
            guard let dataItem = viewModel.data?.get(selectedIndexPath.row) else { return }
            
            destinationViewController.viewModel.id = dataItem.id
            destinationViewController.viewModel.selectedColor = dataItem.color
            destinationViewController.viewModel.selectedItemName = dataItem.name
            
        }
        
    }

}
