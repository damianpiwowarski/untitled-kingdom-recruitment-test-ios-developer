//
//  ItemDetailsModelTests.swift
//  Recruitment-iOSTests
//
//  Created by Damian Piwowarski on 20/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import XCTest

@testable import Recruitment_iOS

class ItemDetailsModelTests: XCTestCase {
    
    func testParsing() {
        
        let testBundle = Bundle(for: type(of: self))
        
        guard let filepath = testBundle.path(forResource: "TestItemDetails", ofType: "json") else {
            XCTFail("Can't find TestItemDetails.json")
            return
        }
        
        do {
            
            let stringContent = try String(contentsOfFile: filepath, encoding: String.Encoding.utf8)
            guard let data = stringContent.data(using: String.Encoding.utf8) else {
                XCTFail("Can't decode TestItemDetails.json")
                return
            }
            
            let itemModel = try JSONDecoder().decode(ItemDetailsModel.self, from: data)
            
            XCTAssertNotNil(itemModel.desc)
            
            let encodedModel = try JSONEncoder().encode(itemModel)
            
            guard let _ = String(data: encodedModel, encoding: .utf8) else {
                XCTFail("Can't encode TestItemDetails")
                return
            }
            
            let mismatch = Data.jsonMismatch(lhs: data, rhs: encodedModel)
            
            if let mismatch = mismatch {
                XCTFail("Two json files are different, difference at position \(mismatch)")
            }

        } catch _ {
            XCTFail("Error ocurred while parsing data")
        }
        
    }
    
}
