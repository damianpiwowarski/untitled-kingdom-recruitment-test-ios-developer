//
//  ItemModelTests.swift
//  Recruitment-iOSTests
//
//  Created by Damian Piwowarski on 20/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import XCTest

@testable import Recruitment_iOS

class ItemModelTests: XCTestCase {

    func testParsing() {
        
        let testBundle = Bundle(for: type(of: self))
        
        guard let filepath = testBundle.path(forResource: "TestItem", ofType: "json") else {
            XCTFail("Can't find TestItem.json")
            return
        }
        
        do {
            
            let stringContent = try String(contentsOfFile: filepath, encoding: String.Encoding.utf8)
            guard let data = stringContent.data(using: String.Encoding.utf8) else {
                XCTFail("Can't decode TestItem.json")
                return
            }
            
            let itemModel = try JSONDecoder().decode(ItemModel.self, from: data)
            
            // Test extra attributes for more code coverage
            XCTAssertEqual(itemModel.preview, itemModel.attributes.preview)
            XCTAssertEqual(itemModel.color, itemModel.attributes.color.uicolor)
            
            let encodedModel = try JSONEncoder().encode(itemModel)
            
            guard let _ = String(data: encodedModel, encoding: .utf8) else {
                XCTFail("Can't encode ItemModel")
                return
            }
            
            let mismatch = Data.jsonMismatch(lhs: data, rhs: encodedModel)
            
            if let mismatch = mismatch {
                XCTFail("Two json files are different, difference at position \(mismatch)")
            }
            
        } catch _ {
            XCTFail("Error ocurred while parsing data")
        }
        
    }
    
    struct ColorTest {
        let knownColor: Color
        let unknownColor: Color
    }
    
    func testColorParsing() {
        
        do {
            
            let greenColor = "Green"
            
            guard let greenColorData = greenColor.data(using: String.Encoding.utf8) else { XCTFail("Can't decode color from json"); return }
            var decodedColor = try JSONDecoder().decode(Color.self, from: greenColorData)
            
            XCTAssertEqual(decodedColor.name, greenColor)
            XCTAssertEqual(decodedColor.uicolor, UIColor.green)
            
            let mintColor = "Mint"
            guard let unknownColorData = mintColor.data(using: String.Encoding.utf8) else { XCTFail("Can't decode color from json"); return }
            decodedColor = try JSONDecoder().decode(Color.self, from: unknownColorData)
            
            XCTAssertEqual(decodedColor.name, mintColor)
            XCTAssertEqual(decodedColor.uicolor, UIColor.black)
            
            let encodedGreenColor = try JSONEncoder().encode(greenColorData)
            guard let encodedGreenColorString = String(data: encodedGreenColor, encoding: .utf8) else {
                XCTFail("Can't encode Color back to green")
                return
            }
            
            XCTAssertEqual(encodedGreenColorString, greenColor)
            
        } catch let error {
            print(error)
        }
    
    }
    
}
