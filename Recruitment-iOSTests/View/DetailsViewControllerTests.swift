//
//  DetailsViewControllerTests.swift
//  Recruitment-iOSTests
//
//  Created by Damian Piwowarski on 17/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import XCTest

@testable import Recruitment_iOS
 
class DetailsViewControllerTests: XCTestCase {
    
    func testDetailsViewController() {
        
        XCTAssertEqual(isLeaking(), false)
        
    }
    
    func isLeaking() -> Bool {
        
        var controller: DetailsViewController? = DetailsViewController()
        weak var leakCheck = controller
        controller = nil
        
        if leakCheck == nil {
            return false
        }
        
        return true
        
    }

}
