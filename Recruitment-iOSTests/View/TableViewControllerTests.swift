//
//  TableViewControllerTests.swift
//  Recruitment-iOSTests
//
//  Created by Damian Piwowarski on 17/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import XCTest

@testable import Recruitment_iOS

class TableViewControllerTests: XCTestCase {
    
    var controller: TableViewController!
    
    override func setUp() {
        super.setUp()
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        controller = storyBoard.instantiateViewController(withIdentifier: TableViewController.stringName) as? TableViewController
        UIApplication.shared.keyWindow?.rootViewController = controller
        controller.loadViewIfNeeded()
    }
    
    func testTableViewController() {
        
        XCTAssertEqual(isLeaking(), false)
        XCTAssertNotNil(controller?.tableView)
        
    }

    // Test how segue performs with real data
    // We dont check segue id here, only what data is sent
    func testPerformSegue() {

        let data = [ createSampleItemModel() ]
        controller.viewModel.data = data
        controller.tableView.selectRow(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: .none)
        controller.tableView(controller.tableView, didSelectRowAt: IndexPath(row: 0, section: 0))
        
        if let presentedDetailsViewController = controller.presentedViewController as? DetailsViewController {

            // Check if presented data in DetailsViewController is the same as sample model
            let sampleData = data[0]

            XCTAssertEqual(sampleData.name, presentedDetailsViewController.viewModel.selectedItemName)
            XCTAssertEqual(sampleData.color, presentedDetailsViewController.viewModel.selectedColor)
            XCTAssertEqual(sampleData.id, presentedDetailsViewController.viewModel.id)

        } else {
            XCTFail("Controller presented wrong View Controller")
        }

    }
    
    // Test which segue id will be selected with selected data
    // Segue will not perform in MockTableViewController, we only get its id
    func testUsedSegue() {
        
        let controller = MockTableViewController()
        
        // With no data, there should be no segue
        var expectedSegueIdentifier: String? = nil
        controller.viewModel.data = nil
        controller.tableView(controller.tableView, didSelectRowAt: IndexPath(row: 0, section: 0))
        XCTAssertEqual(controller.usedSegueIdentifier, expectedSegueIdentifier)
        
        // Create data and test segue
        let data = [ createSampleItemModel() ]
        controller.viewModel.data = data
        expectedSegueIdentifier = "ShowDetailsSegue"
        controller.tableView(controller.tableView, didSelectRowAt: IndexPath(row: 0, section: 0))
        XCTAssertEqual(controller.usedSegueIdentifier, expectedSegueIdentifier)
        
    }
    
    func isLeaking() -> Bool {
        
        var controller: TableViewController? = TableViewController()
        weak var leakCheck = controller
        controller = nil
        
        if leakCheck == nil {
            return false
        }
        
        return true
        
    }
    
    // MARK: - Helping methods
    
    func createSampleItemModel() -> ItemModel {
        
        let color = Color(name: "Black", uiColor: UIColor.black)
        let attributes = ItemModelAttributes(name: "Sample", color: color, preview: nil, desc: nil)
        let itemModel = ItemModel(id: "1", type: "Item", attributes: attributes)
        
        return itemModel
        
    }
    
    class MockTableViewController: TableViewController {
        
        var usedSegueIdentifier: String?
        
        override func performSegue(withIdentifier identifier: String, sender: Any?) {
            self.usedSegueIdentifier = identifier
        }
        
    }
    
}
