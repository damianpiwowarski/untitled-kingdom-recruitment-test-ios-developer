//
//  TableViewModelTests.swift
//  Recruitment-iOSTests
//
//  Created by Damian Piwowarski on 17/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import XCTest

@testable import Recruitment_iOS

class TableViewViewModelTests: XCTestCase, TableViewModelDelegate {

    let viewModel = TableViewViewModel()
    var currentCheck: DataLoadCheck?
    var currentExpectation: XCTestExpectation?
    
    override func setUp() {
        viewModel.delegate = self
    }
    
    override func tearDown() {
        viewModel.delegate = nil
    }
    
    // MARK: - TableViewModelDelegate
    
    func dataLoad() {
        
        currentExpectation?.fulfill()
        
        if currentCheck != DataLoadCheck.checkLoadSuccess {
            XCTFail("Wrong data callback called")
        }
        
    }
    
    func dataLoadError() {
        
    }
    
    // MARK: - Tests TableViewModel
    
    func testDataLoad() {
        
        // Check for success callback
        currentCheck = .checkLoadSuccess
        currentExpectation = expectation(description: "Check data load success")
        viewModel.getData()
        waitForExpectations(timeout: 10, handler: nil)
        
    }
    
    func testTableViewModel() {
        
        XCTAssertEqual(isLeaking(), false)
        
    }
    
    func isLeaking() -> Bool {
        
        var controller: TableViewViewModel? = TableViewViewModel()
        weak var leakCheck = controller
        controller = nil
        
        if leakCheck == nil {
            return false
        }
        
        return true
        
    }

}
