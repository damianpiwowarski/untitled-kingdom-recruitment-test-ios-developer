//
//  TableViewModelTests.swift
//  Recruitment-iOSTests
//
//  Created by Damian Piwowarski on 17/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import XCTest

@testable import Recruitment_iOS

enum DataLoadCheck {
    case checkLoadSuccess
    case checkLoadError
}

class DetailsViewViewModelTests: XCTestCase, DetailsViewModelDelegate {
    
    let viewModel = DetailsViewViewModel()
    var currentCheck: DataLoadCheck?
    var currentExpectation: XCTestExpectation?
    
    override func setUp() {
        viewModel.delegate = self
    }
    
    override func tearDown() {
        viewModel.delegate = nil
    }
    
    // MARK: - DetailsViewModelDlegate
    
    func dataLoad() {
        
        currentExpectation?.fulfill()
        
        if currentCheck != DataLoadCheck.checkLoadSuccess {
            XCTFail("Wrong data callback called")
        }
        
    }
    
    func dataLoadError() {
        
        currentExpectation?.fulfill()
        
        if currentCheck != DataLoadCheck.checkLoadError {
            XCTFail("Wrong data callback called")
        }
        
    }
    
    // MARK: - Tests DetailsViewModel
    
    func testDetailsViewModel() {
        
        XCTAssertEqual(isLeaking(), false)
        
    }
    
    func testDataLoad() {
        
        // Check for error callback
        currentCheck = .checkLoadError
        currentExpectation = expectation(description: "Check data load error")
        viewModel.id = nil
        viewModel.getData()
        waitForExpectations(timeout: 10, handler: nil)
        
        // Check for success callback
        currentCheck = .checkLoadSuccess
        currentExpectation = expectation(description: "Check data load success")
        viewModel.id = "1"
        viewModel.getData()
        waitForExpectations(timeout: 10, handler: nil)
        
    }
    
    func testFormattedTitle() {
        
        viewModel.selectedItemName = nil
        XCTAssertNil(viewModel.formattedTitle)
        
        XCTAssertTrue(checkIfTitleFormatted(original: "Test1", expected: "TeSt1"))
        XCTAssertTrue(checkIfTitleFormatted(original: "TEST1", expected: "TeSt1"))
        XCTAssertTrue(checkIfTitleFormatted(original: "test1", expected: "TeSt1"))
        XCTAssertTrue(checkIfTitleFormatted(original: "Long Title", expected: "LoNg tItLe"))
        
    }
    
    func checkIfTitleFormatted(original: String, expected: String) -> Bool {
        
        viewModel.selectedItemName = original
        return viewModel.formattedTitle == expected
        
    }
    
    func isLeaking() -> Bool {
        
        var controller: DetailsViewViewModel? = DetailsViewViewModel()
        weak var leakCheck = controller
        controller = nil
        
        if leakCheck == nil {
            return false
        }
        
        return true
        
    }
    
}
