//
//  NetworkingManagerTests.swift
//  Recruitment-iOSTests
//
//  Created by Damian Piwowarski on 17/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import XCTest

@testable import Recruitment_iOS

class NetworkingManagerTests: XCTestCase {
    
    func testNetworkingManager() {
        
        XCTAssertEqual(isLeaking(), false)

    }

    // Test if main Items.json file exists and if all children have added proper json details files
    func testCheckIfJsonFilesExist() {
        
        // Check if main list file exists
        XCTAssertTrue(jsonFileExists(fileName: "Items"))
        
        let expectationFileLoad = expectation(description: "Loading items from Items.json")
        
        NetworkingManager.shared.downloadItems { (items, error) in
            
            expectationFileLoad.fulfill()
            
            guard let items = items else { XCTFail("Items.json has no data or data is invalid"); return }
            let filenames = items.map({ $0.name })
            
            for filename in filenames {
                
                if self.jsonFileExists(fileName: filename) == false {
                    XCTFail("File \(filename).json not found")
                }
                
            }
            
        }
        
        waitForExpectations(timeout: 10, handler: nil)
        
    }
    
    // Test if Items.json list loads properly
    func testCheckIfItemsLoad() {
        
        let expectationFileLoad = expectation(description: "Loading items from Items.json")
        
        NetworkingManager.shared.downloadItems { (items, error) in
            
            expectationFileLoad.fulfill()
            if error != nil { XCTFail("Failed to load items from NetworkingManager.shared.downloadItems") }
            
        }
        
        waitForExpectations(timeout: 10, handler: nil)
        
    }
    
    // Test if every detail is serializes properly and if files aren't broken
    func testCheckIfDetailsLoad() {
        
        let expectationFileLoad = expectation(description: "Loading items from Items.json")
        var detailsIds = [String]()
        
        NetworkingManager.shared.downloadItems { (items, error) in

            guard let items = items else { XCTFail("Items.json has no data or data is invalid"); return }
            detailsIds = items.map({ $0.id })
            expectationFileLoad.fulfill()
            
        }
        
        wait(for: [expectationFileLoad], timeout: 10)
        XCTAssertFalse(detailsIds.count == 0)
        
        let expectationDetailsLoad = self.expectation(description: "Loading \(detailsIds.count) details data")
        expectationDetailsLoad.expectedFulfillmentCount = detailsIds.count
        
        for detailsId in detailsIds {
            
            NetworkingManager.shared.downloadItem(withId: detailsId, callback: { (data, error) in
                
                expectationDetailsLoad.fulfill()
                if error != nil { XCTFail("Failed to load details for " + detailsId) }
                
            })
            
        }
        
        wait(for: [expectationDetailsLoad], timeout: TimeInterval(100 * detailsIds.count))
        
    }
    
    // MARK: - Helping methods
    
    func jsonFileExists(fileName: String) -> Bool {
        return Bundle.main.path(forResource: fileName, ofType: "json") != nil
    }
    
    func isLeaking() -> Bool {
        
        var controller: NetworkingManager? = NetworkingManager()
        weak var leakCheck = controller
        controller = nil
        
        if leakCheck == nil {
            return false
        }
        
        return true
        
    }

}
