//
//  JSONParserTests.swift
//  Recruitment-iOSTests
//
//  Created by Damian Piwowarski on 18/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import XCTest

@testable import Recruitment_iOS

class JSONParserTests: XCTestCase {
    
    func testJSONParser() {
        
        XCTAssertEqual(isLeaking(), false)
        
    }
    
    func isLeaking() -> Bool {
        
        var controller: JSONParser? = JSONParser()
        weak var leakCheck = controller
        controller = nil
        
        if leakCheck == nil {
            return false
        }
        
        return true
        
    }

}
